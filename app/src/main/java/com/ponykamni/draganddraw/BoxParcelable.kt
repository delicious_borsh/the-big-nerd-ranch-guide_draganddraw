package com.ponykamni.draganddraw

import android.graphics.PointF
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class BoxParcelable(
    private val left: Float,
    private val right: Float,
    private val top: Float,
    private val bottom: Float
) : Parcelable {

    fun toBox(): Box {
        val box = Box(
            PointF(left, top)
        )
        box.end = PointF(this.right, this.bottom)

        return box
    }

    companion object {

        fun fromBox(box: Box): BoxParcelable {
            return BoxParcelable(
                box.left,
                box.right,
                box.top,
                box.bottom
            )
        }
    }
}