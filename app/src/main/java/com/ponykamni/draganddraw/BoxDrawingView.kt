package com.ponykamni.draganddraw

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.os.Parcel
import android.os.Parcelable
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import kotlin.math.atan2


class BoxDrawingView(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {

    private var currentBox: Box? = null
    private val boxen = mutableListOf<Box>()

    private val boxPaint = Paint().apply {
        color = 0x22ff0000.toInt()
    }
    private val backgroundPaint = Paint().apply {
        color = 0xfff8efe0.toInt()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val (boxStartPoint, anglePoint) = initTouchPointOnActionEvent(event)

        var action = ""
        when (event?.actionMasked!!) {
            MotionEvent.ACTION_DOWN -> {
                action = "ACTION_DOWN"
                // Reset drawing state
                actionDown(boxStartPoint)
            }
            MotionEvent.ACTION_POINTER_DOWN -> {
                action = "ACTION_DOWN"
                actionPointerDown(anglePoint)
            }
            MotionEvent.ACTION_MOVE -> {
                action = "ACTION_MOVE"
                actionMove(boxStartPoint, anglePoint)
            }
            MotionEvent.ACTION_UP -> {
                action = "ACTION_UP"
                actionUp(boxStartPoint)
                sendAccessibilityEvent(event)
            }
            MotionEvent.ACTION_CANCEL -> {
                action = "ACTION_CANCEL"
                actionCancel()
            }
        }

        Log.i(
            TAG,
            "$action at x=${boxStartPoint?.x}, y=${boxStartPoint?.y}"
        )

        return true
    }

    override fun onDraw(canvas: Canvas?) {

        canvas!!.drawPaint(backgroundPaint)

        boxen.forEach { box ->
            val angle = box.angle
            val px = (box.start.x + box.end.x) / 2
            val py = (box.start.y + box.end.y) / 2
            canvas.apply {
                save()
                rotate(angle, px, py)
                drawRect(
                    box.left, box.top,
                    box.right, box.bottom,
                    boxPaint
                )
                restore()
            }
        }
    }

    override fun onSaveInstanceState(): Parcelable? {
        val savedState = SavedState(super.onSaveInstanceState())

        val boxesParcelableArrayList = ArrayList<Parcelable>(boxen.map {
            BoxParcelable.fromBox(it)
        })

        savedState.boxesParcelableList = boxesParcelableArrayList
        return savedState
    }

    override fun onRestoreInstanceState(state: Parcelable) {
        if (state is SavedState) {
            super.onRestoreInstanceState(state.superState)

            boxen.addAll(state.boxesParcelableList.map {
                (it as BoxParcelable).toBox()
            })

        } else {
            super.onRestoreInstanceState(state)
        }
    }

    internal class SavedState(superState: Parcelable?) : BaseSavedState(superState) {

        var boxesParcelableList = ArrayList<Parcelable>()

        override fun writeToParcel(out: Parcel, flags: Int) {
            super.writeToParcel(out, flags)
            out.writeParcelableList(boxesParcelableList, 0)
        }
    }

    private fun sendAccessibilityEvent(event: MotionEvent?) {
        sendAccessibilityEvent(event!!.action)
        this.contentDescription = "There are ${boxen.size} boxes on the screen"
    }

    private fun updateCurrentBox(current: PointF) {
        currentBox?.let {
            it.end = current
            invalidate()
        }
    }

    private fun updateCurrentBoxAngle(anglePoint: PointF) {
        currentBox?.let {
            val boxOrigin = it.start
            val pointerOrigin = it.anglePointer
            val angle2 =
                atan2(
                    (anglePoint.y - boxOrigin.y).toDouble(),
                    (anglePoint.x - boxOrigin.x).toDouble()
                )
                    .toFloat()
            val angle1 =
                atan2(
                    (pointerOrigin.y - boxOrigin.y).toDouble(),
                    (pointerOrigin.x - boxOrigin.x).toDouble()
                )
                    .toFloat()
            var calculatedAngle =
                Math.toDegrees((angle2 - angle1).toDouble())
                    .toFloat()
            if (calculatedAngle < 0) calculatedAngle += 360f
            it.angle = calculatedAngle

            Log.d(TAG, "Set Box Angle $calculatedAngle");
            invalidate()
        }
    }

    private fun actionCancel() {
        currentBox = null
    }

    private fun actionUp(boxStartPoint: PointF?) {
        boxStartPoint?.let {
            updateCurrentBox(boxStartPoint)
        }
        currentBox = null
    }

    private fun actionMove(
        boxStartPoint: PointF?,
        anglePoint: PointF?
    ) {
        boxStartPoint?.let {
            updateCurrentBox(boxStartPoint)
        }
        anglePoint?.let {
            updateCurrentBoxAngle(anglePoint)
        }
    }

    private fun actionPointerDown(anglePoint: PointF?) {
        currentBox?.let { box ->
            anglePoint?.let {
                box.anglePointer = anglePoint
            }
        }
    }

    private fun actionDown(boxStartPoint: PointF?) {
        boxStartPoint?.let {
            currentBox = Box(boxStartPoint).also {
                boxen.add(it)
            }
        }
    }

    private fun initTouchPointOnActionEvent(
        event: MotionEvent?
    ): Pair<PointF?, PointF?> {

        var touchPoint: PointF? = null
        var touchPoint2: PointF? = null

        for (i in 0 until event!!.pointerCount) {

            if (event.getPointerId(i) == 0)
                touchPoint = PointF(event.x, event.y)

            if (event.getPointerId(i) == 1)
                touchPoint2 = PointF(event.x, event.y)
        }
        return touchPoint to touchPoint2
    }

    companion object {

        private const val TAG = "BoxDrawingView"
    }
}