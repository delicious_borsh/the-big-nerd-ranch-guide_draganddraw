package com.ponykamni.draganddraw

import android.graphics.PointF

class Box(val start: PointF) {
    
    var angle: Float = 0.0F

    var anglePointer: PointF = PointF(0.0F, 0.0F)

    var end: PointF = start

    val left: Float
        get() = Math.min(start.x, end.x)
    val right: Float
        get() = Math.max(start.x, end.x)
    val top: Float
        get() = Math.min(start.y, end.y)
    val bottom: Float
        get() = Math.max(start.y, end.y)
}